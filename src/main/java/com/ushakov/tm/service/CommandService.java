package com.ushakov.tm.service;

import com.ushakov.tm.api.repository.ICommandRepository;
import com.ushakov.tm.api.service.ICommandService;
import com.ushakov.tm.model.Command;

public class CommandService implements ICommandService {

    private final ICommandRepository commandRepository;

    public CommandService(ICommandRepository commandRepository) {
        this.commandRepository = commandRepository;
    }

    @Override
    public Command[] getTerminalCommands() {
        return commandRepository.getTerminalCommands();
    }

}
