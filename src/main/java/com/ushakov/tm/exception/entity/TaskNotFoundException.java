package com.ushakov.tm.exception.entity;

public class TaskNotFoundException extends Exception {

    public TaskNotFoundException() {
        super("Error! Task was not found!");
    }

}
