package com.ushakov.tm.exception.empty;

public class EmptyDescriptionException extends Exception {

    public EmptyDescriptionException() {
        super("Error! Description is empty!");
    }

}
