package com.ushakov.tm.exception.empty;

public class EmptyIndexException extends Exception {

    public EmptyIndexException() {
        super("Error! Index is empty!");
    }

}
