package com.ushakov.tm;

import com.ushakov.tm.bootstrap.Bootstrap;
import com.ushakov.tm.exception.system.UnknownArgumentException;

public class Application {

    public static void main(String[] args) throws UnknownArgumentException {
        final Bootstrap bootstrap = new Bootstrap();
        bootstrap.run(args);
    }

}
