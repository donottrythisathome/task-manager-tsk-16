package com.ushakov.tm.api.service;

import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.List;

public interface IProjectTaskService {

    // TASK CONTROLLER
    List<Task> findAllTasksByProjectId(String projectId) throws EmptyIdException;

    // TASK CONTROLLER
    Task bindTaskByProjectId(String projectId, String taskId) throws TaskNotFoundException, EmptyIdException, ProjectNotFoundException;

    // TASK CONTROLLER
    Task unbindTaskFromProject(String taskId) throws EmptyIdException, TaskNotFoundException;

    // PROJECT CONTROLLER
    Project deleteProjectById(String projectId) throws TaskNotFoundException, ProjectNotFoundException, EmptyIdException;

}
