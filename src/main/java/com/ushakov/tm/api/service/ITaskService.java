package com.ushakov.tm.api.service;

import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.empty.EmptyDescriptionException;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface ITaskService {

    List<Task> findAll();

    List<Task> findAll(Comparator<Task> comparator);

    Task add(String name, String description) throws EmptyDescriptionException, EmptyNameException;

    void add(Task task) throws TaskNotFoundException;

    void remove(Task task) throws TaskNotFoundException;

    void clear();

    Task removeOneByName(String name) throws TaskNotFoundException, EmptyNameException;

    Task removeOneById(String id) throws TaskNotFoundException, EmptyIdException;

    Task removeOneByIndex(Integer index) throws EmptyIndexException, TaskNotFoundException;

    Task findOneByName(String name) throws EmptyNameException;

    Task findOneById(String id) throws EmptyIdException;

    Task findOneByIndex(Integer index) throws EmptyIndexException, TaskNotFoundException;

    Task updateTaskByIndex(Integer index, String name, String description) throws TaskNotFoundException, EmptyNameException, EmptyIndexException;

    Task updateTaskById(String id, String name, String description) throws TaskNotFoundException, EmptyNameException, EmptyIdException;

    Task startTaskById(String id) throws TaskNotFoundException, EmptyIdException;

    Task startTaskByName(String name) throws TaskNotFoundException, EmptyNameException;

    Task startTaskByIndex(Integer index) throws TaskNotFoundException, EmptyIndexException;

    Task completeTaskById(String id) throws TaskNotFoundException, EmptyIdException;

    Task completeTaskByName(String name) throws TaskNotFoundException, EmptyNameException;

    Task completeTaskByIndex(Integer index) throws TaskNotFoundException, EmptyIndexException;

    Task changeTaskStatusById(String id, Status status) throws TaskNotFoundException, EmptyIdException;

    Task changeTaskStatusByName(String name, Status status) throws TaskNotFoundException, EmptyNameException;

    Task changeTaskStatusByIndex(Integer index, Status status) throws TaskNotFoundException, EmptyIndexException;

}
