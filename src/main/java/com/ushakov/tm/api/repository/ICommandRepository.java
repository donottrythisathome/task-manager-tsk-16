package com.ushakov.tm.api.repository;

import com.ushakov.tm.model.Command;

public interface ICommandRepository {

    Command[] getTerminalCommands();

}
