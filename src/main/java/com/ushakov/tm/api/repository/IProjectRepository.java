package com.ushakov.tm.api.repository;

import com.ushakov.tm.enumerated.Status;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.model.Project;
import com.ushakov.tm.model.Task;

import java.util.Comparator;
import java.util.List;

public interface IProjectRepository {

    List<Project> findAll();

    List<Project> findAll(Comparator<Project> comparator);

    void add(Project project);

    void remove(Project project);

    void clear();

    Project removeOneByName(String name) throws ProjectNotFoundException;

    Project removeOneById(String id) throws ProjectNotFoundException;

    Project removeOneByIndex(Integer index) throws ProjectNotFoundException;

    Project findOneByName(String name);

    Project findOneById(String id);

    Project findOneByIndex(Integer index) throws ProjectNotFoundException;

}
