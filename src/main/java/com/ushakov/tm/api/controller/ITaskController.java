package com.ushakov.tm.api.controller;

import com.ushakov.tm.exception.empty.EmptyDescriptionException;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.exception.system.IndexIncorrectException;
import com.ushakov.tm.model.Task;

import java.util.List;

public interface ITaskController {

    void showList();

    void create() throws TaskNotFoundException, EmptyNameException, EmptyDescriptionException;

    void clear();

    void removeTaskById() throws TaskNotFoundException, EmptyIdException;

    void removeTaskByName() throws TaskNotFoundException, EmptyNameException;

    void removeTaskByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException;

    void findTaskById() throws TaskNotFoundException, EmptyIdException;

    void findTaskByName() throws TaskNotFoundException, EmptyNameException;

    void findTaskByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException;

    void updateTaskByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException, EmptyNameException;

    void updateTaskById() throws TaskNotFoundException, EmptyIdException, EmptyNameException;

    void startTaskById() throws TaskNotFoundException, EmptyIdException;

    void startTaskByName() throws TaskNotFoundException, EmptyNameException;

    void startTaskByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException;

    void completeTaskById() throws TaskNotFoundException, EmptyIdException;

    void completeTaskByName() throws TaskNotFoundException, EmptyNameException;

    void completeTaskByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException;

    void changeTaskStatusById() throws TaskNotFoundException, EmptyIdException;

    void changeTaskStatusByName() throws TaskNotFoundException, EmptyNameException;

    void changeTaskStatusByIndex() throws TaskNotFoundException, IndexIncorrectException, EmptyIndexException;

    void findAllTasksByProjectId() throws TaskNotFoundException, EmptyIdException;

    void bindTaskByProjectId() throws TaskNotFoundException, EmptyIdException, ProjectNotFoundException;

    void unbindTaskFromProject() throws TaskNotFoundException, EmptyIdException;

}
