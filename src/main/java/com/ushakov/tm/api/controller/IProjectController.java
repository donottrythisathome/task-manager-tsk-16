package com.ushakov.tm.api.controller;

import com.ushakov.tm.exception.empty.EmptyDescriptionException;
import com.ushakov.tm.exception.empty.EmptyIdException;
import com.ushakov.tm.exception.empty.EmptyIndexException;
import com.ushakov.tm.exception.empty.EmptyNameException;
import com.ushakov.tm.exception.entity.ProjectNotFoundException;
import com.ushakov.tm.exception.entity.TaskNotFoundException;
import com.ushakov.tm.exception.system.IndexIncorrectException;

public interface IProjectController {

    void showList();

    void create() throws ProjectNotFoundException, EmptyNameException, EmptyDescriptionException;

    void clear();

    void removeProjectById() throws ProjectNotFoundException, EmptyIdException;

    void removeProjectByName() throws ProjectNotFoundException, EmptyNameException;

    void removeProjectByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException;

    void findProjectById() throws ProjectNotFoundException, EmptyIdException;

    void findProjectByName() throws ProjectNotFoundException, EmptyNameException;

    void findProjectByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException;

    void updateProjectByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException, EmptyNameException;

    void updateProjectById() throws ProjectNotFoundException, EmptyIdException, EmptyNameException;

    void startProjectById() throws ProjectNotFoundException, EmptyIdException;

    void startProjectByName() throws ProjectNotFoundException, EmptyNameException;

    void startProjectByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException;

    void completeProjectById() throws ProjectNotFoundException, EmptyIdException;

    void completeProjectByName() throws ProjectNotFoundException, EmptyNameException;

    void completeProjectByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException;

    void changeProjectStatusById() throws ProjectNotFoundException, EmptyIdException;

    void changeProjectStatusByName() throws ProjectNotFoundException, EmptyNameException;

    void changeProjectStatusByIndex() throws ProjectNotFoundException, IndexIncorrectException, EmptyIndexException;

    void deleteProjectById() throws ProjectNotFoundException, EmptyIdException, TaskNotFoundException;

}
